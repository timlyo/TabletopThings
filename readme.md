TabletopThings is a website designed as a curated list of useful tools for tabletop games.

# Goals

All tools should contain information about:

- Applicable Games 
- Cost
- Open Source
- Mobile App, Desktop App, or website
- Category of tool
- Tag within category

# Categories

To be refined in future

- Generation
    - Character, world, loot
- Tools
    - Dice, information
- Information
    - Encyclopedia