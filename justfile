watch_interface:
	watchexec -c -w src -- just interface

interface: dir html sass
	elm-make src/elm/Main.elm --output output/Main.js --debug
	@notify-send "Compiled"

format:
	elm-format src/*.elm --yes
	elm-format tests/*.elm --yes

html: dir
	cp src/index.html output/index.html

sass: dir
	sassc src/style.sass output/style.css

dir:
	mkdir -p output

start_server:
	cd output; caddy

test:
	node_modules/elm-test/bin/elm-test tests/*.elm

watch_test:
	node_modules/elm-test/bin/elm-test tests/*.elm --watch
