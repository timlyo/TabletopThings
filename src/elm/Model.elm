module Model exposing (..)

import Things exposing (..)

type alias Model =
    { things : List Thing
    , platforms : List Platform
    , selected_game : Game
    , selected_tag : Maybe String
    }
