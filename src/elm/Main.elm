module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Debug exposing (log)
import Set exposing (toList, Set)
import Select exposing (..)
import Things exposing (..)
import Model exposing (..)


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }




init : ( Model, Cmd Msg )
init =
    ( { things = things
      , platforms = []
      , selected_game = Any
      , selected_tag = Nothing
      }
    , Cmd.none
    )


type Msg
    = Name String
    | PlatformChanged Platform
    | GameChanged Game
    | TagChanged String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case (log "msg" msg) of
        Name name ->
            ( model, Cmd.none )

        PlatformChanged platform ->
            ( add_platform model platform
            , Cmd.none
            )

        GameChanged game ->
            ( { model | selected_game = game }
            , Cmd.none
            )

        TagChanged tag ->
            ( if model.selected_tag == Just tag then
                { model | selected_tag = Nothing }
              else
                { model | selected_tag = Just tag }
            , Cmd.none
            )


add_platform : Model -> Platform -> Model
add_platform model platform =
    if List.member platform model.platforms then
        { model | platforms = List.filter (\p -> p /= platform) model.platforms }
    else
        { model | platforms = platform :: model.platforms }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


any_same : List a -> List a -> Bool
any_same left right =
    let
        matches =
            List.filter (\x -> List.member x right) left
    in
        List.length matches > 0



-- View Rendering


view : Model -> Html Msg
view model =
    div [ id "container" ]
        [ div [ class "horizontal-split" ]
            [ div [ class "box margin-right" ]
                [ h1 [] [ text "Tabletop Things" ]
                , render_tag_box (get_tag_list model.things)
                ]
            , div [] [ render_filters, render_thing_list model ]
            ]
        ]


{-| Determine if a thing should be in the list based upon
selected_game in thing.game
|selected_platform ∩ thing.platforms| > 0
tag in thing.tags
-}
filter_thing : Model -> Thing -> Bool
filter_thing model thing =
    let
        game_match = case model.selected_game of
            Any -> True
            _ -> List.member model.selected_game thing.game

        platform_match = case model.platforms of
            [] -> True
            _ -> any_same model.platforms thing.platforms

        tag_match =
            case model.selected_tag of
                Just tag ->
                    List.member tag thing.tags

                -- No tag selected, all tags match
                Nothing ->
                    True
    in
        game_match && platform_match && tag_match


render_thing_list model =
    let
        things =
            List.filter (filter_thing model) model.things
    in
        div [ id "tool-list" ] (List.map render_thing things)


render_thing : Thing -> Html Msg
render_thing thing =
    a [ href thing.url, class "link" ]
        [ h2 [] [ text thing.name ]
        , render_tag_list thing.tags
        , render_platforms thing.platforms
        ]


render_filters =
    div [ id "filters" ]
        [ labeled_checkbox "Web" (PlatformChanged Web)
        , labeled_checkbox "Android" (PlatformChanged Android)
        , labeled_checkbox "iOS" (PlatformChanged IOS)
        , labeled_checkbox "Desktop" (PlatformChanged Desktop)
        , render_game_selector
        ]


render_game_selector =
    label []
        [ text "Game"
        , Select.from [ Any, Pathfinder, FiveE, ThreeFive ] GameChanged
        ]


labeled_checkbox label_text msg =
    label []
        [ text label_text
        , input [ type_ "checkbox", onClick msg ] []
        ]


render_tag_list tags =
    div [ id "tag-list" ] (List.map render_tag tags)


render_tag tag =
    span [ class "tag" ] [ text tag ]


{-| Render list of tags for left hand side
-}
render_tag_box : TagRecord -> Html Msg
render_tag_box tags =
    div [ id "tag-box" ]
        [ h2 [] [ text "Tags" ]
        , ul [ class "no-style" ]
            [ render_tag_section "Generation" tags.generation
            , render_tag_section "Tool" tags.tool
            , render_tag_section "Information" tags.information
            ]
        ]


{-| Render a single category of tags
-}
render_tag_section : String -> Set String -> Html Msg
render_tag_section name tags =
    let
        formatted_tags =
            List.map format_tag (Set.toList tags)
    in
        li [ class "tag-section", id (name ++ "-tag-section") ]
            [ span []
                [ h3 [ class "tag-title" ] [ text name ]
                , ul [ class "tag-list no-style" ] formatted_tags
                ]
            ]


format_tag : String -> Html Msg
format_tag tag =
    li []
        [ button [ onClick (TagChanged tag) ] [ text tag ]
        ]


render_platforms platforms =
    span [ class "platform-list" ] (List.map get_platform_logo platforms)


get_platform_logo platform =
    let
        class_name =
            case platform of
                Android ->
                    "fab fa-android"

                IOS ->
                    "fab fa-apple"

                Desktop ->
                    "fas fa-desktop"

                Web ->
                    "fab fa-internet-explorer"
    in
        i [ class class_name ] []
