module Things exposing (..)

import Set exposing (fromList, Set)


type alias Thing =
    { name : String
    , tags : List String
    , free : Bool
    , url : String
    , platforms : List Platform
    , category : Category
    , game : List Game
    }


type Game
    = Any
    | Pathfinder
    | FiveE
    | ThreeFive
    | OtherSpecific String -- Try not to use this


type Category
    = Generation
    | Tool
    | Information


type alias TagRecord =
    { generation : Set String
    , tool : Set String
    , information : Set String
    }


type Platform
    = Android
    | IOS
    | Desktop
    | Web


{-| List of all things
-}
things : List Thing
things =
    [ { name = "Who The Fuck is My DnD Character?"
      , tags = [ "character" ]
      , free = True
      , url = "http://whothefuckismydndcharacter.com/"
      , platforms = [ Web ]
      , category = Generation
      , game = [ Any ]
      }
    , { name = "Initiative Tracker"
      , tags = [ "Initiative" ]
      , free = True
      , url = "http://donjon.bin.sh/d20/initiative/"
      , platforms = [ Web ]
      , category = Tool
      , game = [ Any ]
      }
    , { name = "Roll20"
      , tags = [ "game tool" ]
      , free = True
      , url = "https://roll20.net/"
      , platforms = [ Web ]
      , category = Tool
      , game = [ Any ]
      }
    , { name = "Pathbuilder"
      , tags = [ "character", "character sheet" ]
      , free = True
      , url = "https://play.google.com/store/apps/details?id=com.redrazors.pathbuilder&hl=en_GB"
      , platforms = [ Android ]
      , category = Generation
      , game = [ Pathfinder ]
      }
    , { name = "Kobold Fight Club"
      , tags = [ "encounter" ]
      , free = True
      , url = "http://kobold.club/fight/#/encounter-builder"
      , platforms = [ Web ]
      , category = Generation
      , game = [ FiveE, ThreeFive ]
      }
    , { name = "DnD Character Sheet"
      , tags = [ "character sheet" ]
      , free = True
      , url = "http://dnd.wizards.com/articles/features/character_sheets"
      , platforms = [ Web ]
      , category = Generation
      , game = [ FiveE, ThreeFive ]
      }
    ]
        |> List.sortBy .name


get_tag_list : List Thing -> TagRecord
get_tag_list things =
    { generation = get_tags_by_category things Generation
    , tool = get_tags_by_category things Tool
    , information = get_tags_by_category things Information
    }


get_tags_by_category : List Thing -> Category -> Set String
get_tags_by_category things category =
    things
        |> List.filter (\t -> t.category == category)
        |> List.map (\t -> t.tags)
        |> List.foldr (++) []
        |> fromList
