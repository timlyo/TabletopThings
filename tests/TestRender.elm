module TestRender exposing (..)

import Main exposing (..)
import Things exposing (..)
import Expect
import Fuzz exposing (..)
import Test exposing (..)
import Tuple exposing (first)


default_thing =
    { name = "default", tags = [], free = True, url = "", platforms = [], category = Generation, game = [] }


default_model =
    let
        global_default =
            first init
    in
        { global_default | selected_game = Any, platforms = [], selected_tag = Nothing }


thing_names : List Thing -> List String
thing_names things =
    List.map (\t -> t.name) things


test_filter : Test
test_filter =
    describe "Test filter_thing"
        [ test "Any game, empty platform" <|
            \_ ->
                Expect.true "" (filter_thing default_model default_thing)
        , test "Filter game" <|
            \_ ->
                let
                    model =
                        { default_model | selected_game = Pathfinder }

                    thing =
                        { default_thing | game = [ Pathfinder ] }
                in
                    Expect.true "" (filter_thing model thing)
        , test "Filter platform" <|
            \_ ->
                let
                    model =
                        { default_model | platforms = [ Web ] }

                    thing =
                        { default_thing | platforms = [ Web ] }
                in
                    Expect.true "" (filter_thing model thing)
        ]


test_any_same =
    describe "Test any_same"
        [ test "all_same" <|
            \_ ->
                Expect.true "" (any_same [ 1, 2, 3 ] [ 1, 2, 3 ])
        , test "some_same" <|
            \_ ->
                Expect.true "" (any_same [ 1, 2, 3 ] [ 2, 3 ])
        , test "none_same" <|
            \_ ->
                Expect.false "" (any_same [ 1, 2 ] [ 3, 4 ])
        ]
